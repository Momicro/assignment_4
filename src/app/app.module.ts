import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { ContactsService } from './services/contacts.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LoginPage } from './components/login/login.page';
import { ContactSelectedComponent } from './components/contact-selected/contact-selected.component';
//import { ContactsPage } from './contacts/contacts.page';
import { RegisterPage } from './contact-create/register.page';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import { PokemonSelectedComponent } from './components/pokemon-selected/pokemon-selected.component';
import { PokemonsPage } from './pokemons/pokemons.page';
import { PokemonsTrainerPage } from './components/trainer/pokemons-trainer.page';
import { NgxPaginationModule } from 'ngx-pagination';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginPage,
    ContactSelectedComponent,
    //ContactsPage,
    RegisterPage,
    PokemonListComponent,
    PokemonSelectedComponent,
    PokemonsPage,
    PokemonsTrainerPage
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgxPaginationModule
  ],
  providers: [
    ContactsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
