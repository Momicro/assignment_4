import { Inject, Injectable } from "@angular/core";
//import { ContactListComponent } from "../components/contact-list/contact-list.component";
import { Pokemon } from "../models/pokemon.model";

@Injectable({
  providedIn: 'root'
})
export class SelectedPokemonService {

  //private _contact: Contact = null; Does'nt work!
  private _pokemon: any;

  public setPokemon(pokemon: any) {
    this._pokemon = pokemon;
  }

  public pokemon(): any {
    return this._pokemon;
  }

}
