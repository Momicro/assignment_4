import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Contact } from "../models/contact.model";

@Injectable({
  providedIn: 'root'
})

export class EnrollmentService {

  _url = 'http://localhost:3000/profile';

  constructor(private _http: HttpClient) {}

  enroll(contact: Contact[]) {
    return this._http.post<Contact[]>(this._url, contact);
  }

}

