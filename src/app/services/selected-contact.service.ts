import { Inject, Injectable } from "@angular/core";
//import { ContactListComponent } from "../components/contact-list/contact-list.component";
import { Contact } from "../models/contact.model";

@Injectable({
  providedIn: 'root'
})
export class SelectedContactService {

  //private _contact: Contact = null; Does'nt work!
  private _contact: Contact | null = null;

  public setContact(contact: Contact) {
    this._contact = contact;
  }

  public contact(): Contact | null {
    return this._contact;
  }
}
