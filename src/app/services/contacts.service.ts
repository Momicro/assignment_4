import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Observable, observable } from 'rxjs';
import { Contact } from '../models/contact.model';
import { Pokemon } from '../models/pokemon.model';
import { NgForm } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  private _contacts: Contact[] = [];
  private _pokemons: Pokemon[] = [];
  private _error: string = '';

  // DI - Dependency Injection
  constructor(private readonly http: HttpClient) {
  }

  public fetchContacts(url: string): void {
    //this.http.get<Contact[]>('https://pokeapi.co/api/v2/pokemon?limit=10')
    //this.http.get<Contact[]>(url)
    this.http.get<Contact[]>('http://localhost:3100/contacts')
    .subscribe((contacts: Contact[]) => {
      this._contacts = contacts;
    }, (error: HttpErrorResponse) => {
      this._error = error.message;
    });
  }

  sendContacts(url: string, contact: NgForm) {
    this.http.post<Contact[]>(url, contact);
  }

  public contacts(): Contact[] {
    return this._contacts;
  }

  public error(): string {
    return this._error
  }

  public getPosts(): Observable<any> {
    //return this.http.get('https://pokeapi.co/api/v2/pokemon/ditto')
    return this.http.get('https://jsonplaceholder.typicode.com/posts')
  }

}
