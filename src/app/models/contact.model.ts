//import { Interface } from "readline";

export interface Contact {
  id: number;
  username: string;
  avatar: string;
  name: string;
  email: string;
  password: string;
  address: ContactAddress;
  deleted: boolean;
  createdAt: number;
  loggedIn: boolean;
}

export interface ContactAddress {
  street: string;
  city: string;
  country: string;
}

