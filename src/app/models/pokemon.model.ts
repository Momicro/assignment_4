export interface Pokemon {
  name: string;
  type: string;
  heigh: number;
  health: number;
  power: number;
}
