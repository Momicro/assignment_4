import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPage } from './components/login/login.page';
import { RegisterPage } from './contact-create/register.page';
//import { ContactsPage } from './contacts/contacts.page';
import { PokemonsPage } from './pokemons/pokemons.page';
import { PokemonsTrainerPage } from './components/trainer/pokemons-trainer.page';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login'
  },
  {
    path: 'login',
    component: LoginPage
  },
  {
    path: 'login/create',
    component: RegisterPage
  },
  {
    path: 'login/pokemons',
    component: PokemonsPage
  },
  {
    path: 'login/trainer',
    component: PokemonsTrainerPage
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
