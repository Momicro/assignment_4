import { Component } from "@angular/core";
import { Contact } from "../../models/contact.model";
import { SelectedContactService } from "src/app/services/selected-contact.service";

@Component({
  selector: 'app-contact-selected',
  templateUrl: './contact-selected.component.html',
  styleUrls: ['./contact-selected.component.css']
})

export class ContactSelectedComponent {

  //private _contact: any;

  constructor(private readonly selectedContactService: SelectedContactService) {

  }

  get contact(): Contact | null {
    return this.selectedContactService.contact();

  }

}
