import { Component, OnInit } from '@angular/core';
//import { Pokemon } from 'src/app/models/pokemon.model';
import { DataService } from 'src/app/services/data.service';
import { SelectedPokemonService } from 'src/app/services/selected-pokemon.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit {

  pokemons: any[] = [];
  page = 1;
  totalPokemons: number = 0;

  constructor(private dataService: DataService,
              private selectedPokemonService: SelectedPokemonService
  ) { }

  ngOnInit(): void {
    this.getPokemons();
  }

  // Get Pokemons
  getPokemons() {
    this.dataService.getPokemons(12, this.page + 0)
    .subscribe((response: any) => {
      this.totalPokemons = response.count;

      response.results.forEach((result: any) => {
        this.dataService.getMoreData(result.name)
          .subscribe((uniqResponse: any) => {
            this.pokemons.push(uniqResponse);
            console.log(this.pokemons);
          });
      });
    })
  }

  //onPokemonClicked(pokemon: Pokemon): void {
  onPokemonClicked(pokemon: any): void {
    this.selectedPokemonService.setPokemon(pokemon);
  }

}
