import { Component } from "@angular/core";
import { NgForm } from "@angular/forms";
import { SelectedContactService } from "src/app/services/selected-contact.service";
import { Contact } from "src/app/models/contact.model";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})

export class LoginPage {

  constructor(private readonly selectedContactService: SelectedContactService) {

  }

  get contact(): Contact | null {
    return this.selectedContactService.contact();
  }

  onSubmit(contact: any | boolean | null) {
    if(this.contact?.username === contact.username && this.contact?.password === contact.password) {
      //this.contact.loggedIn = true;
    }
    else{
      //this.contact.loggedIn = false;
    }
  }

}
