import { Component } from "@angular/core";
import { Pokemon } from "src/app/models/pokemon.model";
import { SelectedPokemonService } from "src/app/services/selected-pokemon.service";


@Component({
  selector: 'app-pokemons-trainer',
  templateUrl: './pokemons-trainer.page.html',
  styleUrls: ['./pokemons-trainer.page.css']
})

export class PokemonsTrainerPage {

  page = 1;
  totalPokemons: number = 0;

  constructor(private readonly selectedPokemonService: SelectedPokemonService) {
  }

  get pokemon(): any {
    return this.selectedPokemonService.pokemon();

  }

}
