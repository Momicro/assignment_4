import { Component } from "@angular/core";
import { Pokemon } from "src/app/models/pokemon.model";
import { SelectedPokemonService } from "src/app/services/selected-pokemon.service";


@Component({
  selector: 'app-pokemon-selected',
  templateUrl: './pokemon-selected.component.html',
  styleUrls: ['./pokemon-selected.component.css']
})

export class PokemonSelectedComponent {

  page = 1;
  totalPokemons: number = 0;

  constructor(private readonly selectedPokemonService: SelectedPokemonService) {
  }

  get pokemon(): any {
    return this.selectedPokemonService.pokemon();

  }

}
