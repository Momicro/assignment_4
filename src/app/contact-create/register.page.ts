import { Component } from "@angular/core";
import { NgForm } from "@angular/forms";
import { EnrollmentService } from "../services/enrollment.service";


@Component ({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.css']
})

export class RegisterPage {

  constructor(private _enrollmentService: EnrollmentService) {

  }

  public onSubmit(data: any){
    this._enrollmentService.enroll(data.value)
      .subscribe(
        data => console.log('Success!', data),
        error => console.error('Error!', error)

      )
  }

}
